import pyautogui
import time
time.sleep(2)

# 每个动作间隔0.5秒钟
pyautogui.PAUSE = 0.5
# pyautogui.FAILSAFE = True
# 记事本打出时间
pyautogui.press('f5');
# 打入三行内容
pyautogui.typewrite('\nhelo')
pyautogui.typewrite('\nhelo')
pyautogui.typewrite('\nhelo')

# 按下Ctrl键
pyautogui.keyDown('ctrl');
# 按下a键，拷贝
pyautogui.press('a');
# 按下c键，复制
pyautogui.press('c');
# 松开Ctrl键
pyautogui.keyUp('ctrl')
# 鼠标点击记事本下方
pyautogui.click(600, 600)
# 输入两个空行
pyautogui.typewrite('\n\n')
# 粘贴
pyautogui.hotkey('ctrl', 'v')
